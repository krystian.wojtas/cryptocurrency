#!/usr/bin/ruby

require "monetize"

bcc = <<-JSON
  {
    "priority": 1,
    "iso_code": "BCC",
    "iso_numeric": "000",
    "name": "Bitcoin cash",
    "symbol": "BCC",
    "subunit": "Cent",
    "subunit_to_unit": 10000,
    "symbol_first": true,
    "html_entity": "BCC",
    "decimal_mark": ",",
    "thousands_separator": "."
  }
JSON

dash = <<-JSON
  {
    "priority": 1,
    "iso_code": "DAS",
    "iso_numeric": "000",
    "name": "Dash",
    "symbol": "DASH",
    "subunit": "Cent",
    "subunit_to_unit": 10000,
    "symbol_first": true,
    "html_entity": "DASH",
    "decimal_mark": ",",
    "thousands_separator": "."
  }
JSON

eth = <<-JSON
  {
    "priority": 1,
    "iso_code": "ETH",
    "iso_numeric": "000",
    "name": "Ethereum",
    "symbol": "ETH",
    "subunit": "Cent",
    "subunit_to_unit": 100,
    "symbol_first": true,
    "html_entity": "ETH",
    "decimal_mark": ",",
    "thousands_separator": "."
  }
JSON

ltc = <<-JSON
  {
    "priority": 1,
    "iso_code": "LTC",
    "iso_numeric": "000",
    "name": "Ltc",
    "symbol": "LTC",
    "subunit": "Cent",
    "subunit_to_unit": 100,
    "symbol_first": true,
    "html_entity": "LTC",
    "decimal_mark": ",",
    "thousands_separator": "."
  }
JSON

Money::Currency.register(JSON.parse(bcc, symbolize_names: true))
Money::Currency.register(JSON.parse(dash, symbolize_names: true))
Money::Currency.register(JSON.parse(eth, symbolize_names: true))
Money::Currency.register(JSON.parse(ltc, symbolize_names: true))

# Configure monetize gem printing format
Money.locale_backend = :i18n
I18n.config.available_locales = :en

# Declare functions

def parse_transactions(text)

    text
    .map {
        |line|

        fields = line.split("\s")

        {
            # |
            "pair" \
                => fields[1],
            # |
            "date" \
                => fields[3],
            # |
            "time" \
                => fields[5],
            # |
            "ask_xor_bid" \
                => fields[7],
            # |
            "rate" \
                => Monetize.parse!(
                    #  rate       currency
                    "#{fields[9]} #{fields[10]}"
                ),
            # |
            "amount"  \
                => Monetize.parse!(
                    #  amount      currency
                    # TODO declare LTC as it is not recognized by standard Monetize currencies
                    "#{fields[12]} #{fields[13]}"
                ),
            # |
            "price" \
                => Monetize.parse!(
                    #  price       currency
                    "#{fields[15]} #{fields[16]}"
                ),
            # |
        }
    }

end

def read_input_transactions_and_parse(input)

    # Ommit first line of header
    input.readline

    # Read all rest input lines (TODO is it lazy?)
    text = input.readlines

    # Parse text to structure transactions and return them
    parse_transactions(text)

end

def filter_transactions(transactions, ask_xor_bid)

    transactions.select {
        |transaction|
        transaction["ask_xor_bid"] == ask_xor_bid
    }

end

def sum_amount(transactions)

    transactions
    .map {
        |transaction|
        transaction["amount"]
    }.reduce(:+)

end

def sum_price(transactions)

    transactions
    .map {
        |transaction|
        transaction["price"]
    }.reduce(:+)

end

def print_summary(ask_xor_bid, sum_price, sum_amount, avarage_rate)

        # Set output string formatting
        sum_amount_format = sum_amount.format(
            symbol: "",
            with_currency: true,
        )

        # Set output string formatting
        sum_price_format = sum_price.format(
            symbol: "",
            with_currency: true,
        )

        # Print summary of calculations
        puts "#{ask_xor_bid} #{sum_amount_format} for #{sum_price_format} with average rate %.8f" % avarage_rate

end

def calculate_and_print_summary(transactions, ask_xor_bid)

    # Filter transactions for requested operations type
    transactions_ask_xor_bid = filter_transactions(
        transactions,
        ask_xor_bid,
    )

    # Calculate and cache result in variable
    sum_price = sum_price(transactions_ask_xor_bid)

    # Calculate and cache result in variable
    sum_amount = sum_amount(transactions_ask_xor_bid)

    # Ensure that any ask transaction is found
    unless sum_amount.nil?

        # Calculate avarage exchange ratio individually for ask transactions group
        avarage_rate = \
            sum_price.fractional.to_f \
            / # / Complete char to fix syntax highliting in emacs
            sum_amount.fractional.to_f \

        # Print calculations in set format
        print_summary(ask_xor_bid, sum_price, sum_amount, avarage_rate)

    end

end

# Run script

# Read standard input stream and parse it to build structure with transactions
transactions = read_input_transactions_and_parse($stdin)

# Process transactions and print summary for grouped Ask or Bid types
calculate_and_print_summary(transactions, "Ask")
calculate_and_print_summary(transactions, "Bid")
